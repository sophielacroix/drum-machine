# npm install
# npm run build
# nom run test
# ==> public/index.html
# ==> public/assets/
# ==> public/data/**/*.json
FROM node:10 as builder
WORKDIR /work
COPY package*.json ./
RUN echo '{ "allow_root": true }' > /root/.bowerrc
RUN npm install
COPY . .
RUN npm run build
RUN npm run test

FROM nginx:1.15-alpine
COPY --from=builder /work/public /usr/share/nginx/html
